<?php

class Database
{
    private static $pdo = null;
    private $configurations;
    private $dsn;
    public function __construct()
    {
        $this->configurations = parse_ini_file('config.ini');
        $this->dsn = "mysql:host=" . $this->configurations['host'] . ";dbname=" . $this->configurations['dbname'];
        $this->getConnection($this->dsn, $this->configurations['username'], $this->configurations['password']);
    }
    private function getConnection($dsn, $user, $password)
    {
        if (self::$pdo == null) {
            try {
                self::$pdo = new PDO($dsn, $user, $password);
                self::$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
            } catch (PDOException $e) {
                echo "Error Connecting Database!" . $e->getMessage();
                die();
            }
        }
        return self::$pdo;
    }
    public function select_query($tableName)
    {
        $rs = self::$pdo->query("SELECT * FROM " . $tableName);
        $rows = $rs->fetchAll();
        return $rows;
    }
    public function insert_query($column1, $tableName)
    {
        $query = "INSERT INTO " . $tableName . " (title, checked) VALUES (?, ?)";
        $status = self::$pdo->prepare($query)->execute([$column1, 0]);
        return $status;
    }
    public function delete_query($id, $tableName)
    {
        $query = "DELETE FROM " . $tableName . " WHERE id=?";
        $status = self::$pdo->prepare($query)->execute([$id]);
        return $status;
    }
    public function close_connection()
    {
        self::$pdo = null;
    }
}
