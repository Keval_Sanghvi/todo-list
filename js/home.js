window.onload = () => {
  const submit_btn = document.getElementById("submit-todo");
  const input_title = document.getElementById("title");

  submit_btn.addEventListener("click", function (e) {
    if (input_title.value == "") {
      e.preventDefault();
      alert("Title Cannot Be Empty!");
    }
  });

  $(".delete-button").click(function () {
    let id = $(this).data("id");
    $("#modal-agree-button").attr("href", `delete-todo.php?id=${id}`);
  });

  let checkboxes_parent = document.querySelector(".todos");
  checkboxes_parent.addEventListener("click", function (e) {
    if (e.target.nodeName === "INPUT") {
      let id = e.target.id;
      // remaining
    }
    return;
  });

  function getUrlVars() {
    var vars = [],
      hash;
    var url = window.location.href;
    var queryString = url.slice(url.indexOf("?") + 1);
    var hashes = queryString.split("&");
    for (var i = 0; i < hashes.length; i++) {
      hash = hashes[i].split("=");
      vars[hash[0]] = hash[1];
    }
    return vars;
  }

  var queryStrings = getUrlVars();
  var q = queryStrings["q"];
  var op = queryStrings["op"];
  if (q === "success" && op === "insert") {
    let tag = document.querySelector(".alert-message");
    tag.classList.remove("d-none");
    tag.innerHTML = "Successfully Inserted!!!";
    $(".alert-message").fadeOut(5000);
  } else if (q === "success" && op === "edit") {
    let tag = document.querySelector(".alert-message");
    tag.innerHTML = "Successfully Edited!!!";
    tag.classList.remove("d-none");
    $(".alert-message").fadeOut(5000);
  } else if (q === "success" && op === "delete") {
    let tag = document.querySelector(".alert-message");
    tag.innerHTML = "Successfully Deleted!!!";
    tag.classList.remove("d-none");
    $(".alert-message").fadeOut(5000);
  }
};
