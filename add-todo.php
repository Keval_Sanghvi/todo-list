<?php
require_once("includes/Database.class.php");
if (isset($_POST['action'])) {
    $conn = new Database();
    $title = $_POST['title'];
    $result = $conn->insert_query($title, 'todos');
    if ($result) {
        header('Location: index.php?q=success&op=insert');
    } else {
        header('Location: index.php?q=error&op=insert');
    }
    $conn->close_connection();
}
