<?php
require_once("includes/Database.class.php");
if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $conn = new Database();
    $result = $conn->delete_query($id, 'todos');
    if ($result) {
        header('Location: index.php?q=success&op=delete');
    } else {
        header('Location: index.php?q=error&op=delete');
    }
    $conn->close_connection();
}
