<?php
require_once("includes/Database.class.php");
$conn = new Database();
$rows = $conn->select_query('todos');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Todo List</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap/bootstrap.min.css">
    <!-- Fontawesome -->
    <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
    <!--Custom CSS-->
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <header>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 text-center bg-light">
                    <h3 class="p20 text-dark">Todo List</h3>
                </div>
            </div>
        </div>
    </header>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="status">
                    <div class="alert alert-success alert-message text-center mt15 d-none" role="alert"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form action="add-todo.php" method="POST">
                    <div class="form-group">
                        <label for="title" class="mt30 mb30 todo-title">Add Todo Title</label>
                        <input type="text" class="form-control" id="title" name="title" placeholder="Todo Title" autocomplete="off">
                    </div>
                    <button type="submit" class="btn btn-submit pl20 pr20 mb30" id="submit-todo" name="action">Submit</button>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?php
                if (count($rows) == 0) :
                ?>
                    <div class="no-todos">
                        <h4 class="text-center">No Todos</h4>
                        <img src="images/ellipsis.gif" alt="No Todos">
                    </div>
                <?php
                else :
                ?>
                    <h4 class="text-center">Your Todos</h4>
                    <div class="todos">
                        <?php
                        foreach ($rows as $row) :
                        ?>
                            <div class="todo-item">
                                <input type="checkbox" id="<?= "title" . $row->id; ?>" data-id="<?= $row->id; ?>" class="check">
                                <label for="<?= "title" . $row->id; ?>"><?= $row->title; ?></label>
                                <p><?= "Created on " . $row->date_time; ?></p>
                                <button class="delete-button btn btn-light" data-id="<?= $row->id; ?>" data-toggle="modal" data-target="#delete-modal">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>
                        <?php
                        endforeach;
                        ?>
                    </div>
                <?php
                endif;
                ?>
            </div>
        </div>
    </div>

    <!--Modal-->
    <div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Todo List</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this Todo?</p>
                </div>
                <div class="modal-footer">
                    <a href="#" type="button" class="btn btn-secondary" data-dismiss="modal">No</a>
                    <a href="#" type="button" id="modal-agree-button" class="btn btn-primary">Yes</a>
                </div>
            </div>
        </div>
    </div>
    <!--/Modal-->

    <!-- JQuery -->
    <script src="js/jquery.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="js/bootstrap/bootstrap.min.js"></script>
    <!--Home JS-->
    <script src="js/home.js"></script>
</body>

</html>